# FORBIDDEN MAGIC #

#Uses Witchcraft Doctrine for now
secret_forbidden_magic_is_valid_trigger = {
	$OWNER$ = {
		NOT = {
			has_trait = forbidden_magic_practitioner
		}
	}
}

secret_forbidden_magic_is_shunned_trigger = {
	$OWNER$ = {
		OR = {
			faith = { has_doctrine_parameter = witchcraft_shunned }
			any_liege_or_above = { faith = { has_doctrine_parameter = witchcraft_shunned } }
		}
	}
}

secret_forbidden_magic_is_criminal_trigger = {
	$OWNER$ = {
		OR = {
			faith = { has_doctrine_parameter = witchcraft_illegal }
			any_liege_or_above = { faith = { has_doctrine_parameter = witchcraft_illegal } }
		}
	}
}