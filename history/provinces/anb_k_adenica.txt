769 = { #Rohibon

	# Misc
	culture = rohibonic
	religion = castanorian_pantheon

	# History
}

768 = { #Gallopsway

	# Misc
	culture = rohibonic
	religion = castanorian_pantheon

	# History
}

770 = { #Horsemans_Advance

	# Misc
	culture = rohibonic
	religion = castanorian_pantheon

	# History
}

771 = { #Halanser

	# Misc
	culture = rohibonic
	religion = castanorian_pantheon

	# History
}

763 = { #Listfields

	# Misc
	culture = rohibonic
	religion = castanorian_pantheon

	# History
}

792 = { #Balgarton

	# Misc
	culture = rohibonic
	religion = castanorian_pantheon

	# History
}

765 = { #Tiltwick

	# Misc
	culture = rohibonic
	religion = castanorian_pantheon

	# History
}

764 = { #Upcreek

	# Misc
	culture = rohibonic
	religion = castanorian_pantheon

	# History
}

756 = { #Silvervord

	# Misc
	culture = rohibonic
	religion = castanorian_pantheon

	# History
	1020.1.1 = {
		special_building_slot = white_walls_of_castanor_01
		special_building = white_walls_of_castanor_01
	}
}

767 = { #Feldham

	# Misc
	culture = rohibonic
	religion = castanorian_pantheon

	# History
}

757 = { #Carlanhal

	# Misc
	culture = rohibonic
	religion = castanorian_pantheon

	# History
}

760 = { #Acengard

	# Misc
	culture = rohibonic
	religion = castanorian_pantheon

	# History
}

782 = { #Verteben

	# Misc
	culture = rohibonic
	religion = castanorian_pantheon

	# History
}

781 = { #Shieldrest

	# Misc
	culture = rohibonic
	religion = castanorian_pantheon

	# History
	1020.1.1 = {
		special_building_slot = shieldrest_mines_01
		special_building = shieldrest_mines_01
	}
}

774 = { #Taranton

	# Misc
	culture = rohibonic
	religion = castanorian_pantheon

	# History
}

773 = { #Cantercurse

	# Misc
	culture = rohibonic
	religion = castanorian_pantheon

	# History
}

772 = { #Banwick

	# Misc
	culture = rohibonic
	religion = castanorian_pantheon

	# History
}
