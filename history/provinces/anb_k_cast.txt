746 = { #Acenort

	# Misc
	culture = black_castanorian
	religion = castanorian_pantheon

	# History
	1020.1.1 = {
		special_building_slot = white_walls_of_castanor_01
		special_building = white_walls_of_castanor_01
	}
}

850 = { #Hunters_Folly

	# Misc
	culture = black_castanorian
	religion = castanorian_pantheon

	# History
}

743 = { #Nortwood

	# Misc
	culture = black_castanorian
	religion = castanorian_pantheon

	# History
	1020.1.1 = {
		special_building_slot = white_walls_of_castanor_01
		special_building = white_walls_of_castanor_01
	}
}

849 = { #Khugsroad

	# Misc
	culture = black_castanorian
	religion = castanorian_pantheon

	# History
}

754 = { #Westgate

	# Misc
	culture = castanorian
	religion = castanorian_pantheon

	# History
	1020.1.1 = {
		special_building_slot = white_walls_of_castanor_01
		special_building = white_walls_of_castanor_01
	}
}

842 = { #Venacvord

	# Misc
	culture = castanorian
	religion = castanorian_pantheon

	# History
}

747 = { #Rigelham

	# Misc
	culture = castanorian
	religion = castanorian_pantheon

	# History
	1020.1.1 = {
		special_building_slot = white_walls_of_castanor_01
		special_building = white_walls_of_castanor_01
	}
}

848 = { #Westwood

	# Misc
	culture = castanorian
	religion = castanorian_pantheon

	# History
}

837 = { #Caylens_Fort

	# Misc
	culture = castanorian
	religion = castanorian_pantheon

	# History
}

839 = { #Elderlan

	# Misc
	culture = castanorian
	religion = castanorian_pantheon

	# History
}

847 = { #Glory_Road

	# Misc
	culture = castanorian
	religion = castanorian_pantheon

	# History
}

845 = { #Victors_Path

	# Misc
	culture = castanorian
	religion = castanorian_pantheon

	# History
}

840 = { #The_North_Citadel

	# Misc
	culture = castanorian
	religion = castanorian_pantheon

	# History
	1020.1.1 = {
		special_building_slot = castanorian_citadel_north_citadel_01
		special_building = castanorian_citadel_north_citadel_01
	}
}

843 = { #Trialwood

	# Misc
	culture = castanorian
	religion = castanorian_pantheon

	# History
}

755 = { #Wardenhall

	# Misc
	culture = castanorian
	religion = castanorian_pantheon

	# History
	1020.1.1 = {
		special_building_slot = white_walls_of_castanor_01
		special_building = white_walls_of_castanor_01
	}
}

836 = { #Nurced

	# Misc
	culture = castanorian
	religion = castanorian_pantheon

	# History
}

835 = { #Nortmerewic

	# Misc
	culture = castanorian
	religion = castanorian_pantheon

	# History
}

834 = { #Sapphirewatch

	# Misc
	culture = castanorian
	religion = castanorian_pantheon

	# History
}

846 = { #Serpentswic

	# Misc
	culture = black_castanorian
	religion = castanorian_pantheon

	# History
}

852 = { #Nortmarck

	# Misc
	culture = castanorian
	religion = castanorian_pantheon

	# History
}

858 = { #Shatterwood

	# Misc
	culture = castanorian
	religion = castanorian_pantheon

	# History
}

859 = { #Charwic

	# Misc
	culture = castanorian
	religion = castanorian_pantheon

	# History
}